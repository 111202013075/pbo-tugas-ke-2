package com.tugas;

class Lampu{
    boolean nyala = false;

    void nyalakan(){
        nyala = true;
    }

    void matikan(){
        nyala = false;
    }
}

public class Lampunih {
    public static void main(String[] args) {
	Lampu mylamp = new Lampu();
    mylamp.nyalakan();
    System.out.println("Apakah lampu sudah menyala? " +mylamp.nyala);

    mylamp.matikan();
    System.out.println("Sudah malam matikan lampu: " +mylamp.nyala);
    }
}
